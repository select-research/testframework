//
//  Something.swift
//  TestFramework
//
//  Created by Tom King on 19/08/2019.
//  Copyright © 2019 Tom King. All rights reserved.
//

import Foundation

public class OpenCVHelper
{
    public init()
    {
        
    }
    
    public func calGrabcut()
    {
        let grabcut = BviGrabcutHelperFactory()
        let instance = grabcut.createInstance()
        
        if let normal = UIImage(named: "NORMAL_IMAGE", in: Bundle(for: OpenCVHelper.self), compatibleWith: nil)
        {
            if let mask = UIImage(named: "MASK_IMAGE", in: Bundle(for: OpenCVHelper.self), compatibleWith: nil)
            {
                do
                {
                    let returnImage = try instance.runGrabcut(mask, cropRect: CGRect(x: 0.0, y: 0.0, width: mask.size.width, height: mask.size.height), currentFrame: normal)
                    print("Grabcut Completed Successfully, see console output below for grabcut result image sizes as proof...")
                    print(returnImage.size.height)
                    print(returnImage.size.width)
                }
                catch
                {
                    print("GRABCUT DIDNT WORK")
                }
            }
            else
            {
                print("NO MASK")
            }
        }
        else
        {
            print("NO NORMAL")
        }
    }
}
