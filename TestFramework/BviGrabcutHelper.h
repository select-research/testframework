//
//  BviGrabcutHelper.h
//  BVI SDK
//
//  Created by Jake Wade on 23/07/2019.
//  Copyright © 2019 Jake Wade. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SwiftToObjectiveC.h>

NS_ASSUME_NONNULL_BEGIN

/// Class decleration for BviGrabcutHelper based on the BviGrabcutHelperProtocol
@interface BviGrabcutHelper : NSObject<BviGrabcutHelperProtocol>

    /// The type of the private class that we are going to create
@property (nonatomic, readonly, copy) NSString* privateProperty;

/// Function for running grabcut
///
/// @param grabcutMask The Grabcut mask
/// @param cropRect The grabcut crop rect
/// @param currentFrame The current video frame
/// @param error AKA Throws, just weird objective c syntax
-(UIImage*) runGrabcut: (UIImage*)grabcutMask cropRect:(CGRect)cropRect currentFrame:(UIImage*)currentFrame error:(NSError *__autoreleasing *)error;

@end

NS_ASSUME_NONNULL_END
