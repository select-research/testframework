//
//  NSObject+BviGrabcutHelper.m
//  BVI SDK
//
//  Created by Jake Wade on 23/07/2019.
//  Copyright © 2019 Jake Wade. All rights reserved.
//

#import <opencv2/opencv.hpp>
#import <opencv2/imgcodecs/ios.h>
#import "BviGrabcutHelper.h"



@implementation BviGrabcutHelper

@synthesize privateProperty;

+ (void)load
{
    // Use it to register self class in the "Swift world"
    [BviGrabcutHelperFactory registerPrivateClassTypeWithType:[BviGrabcutHelper class]];
}

/// Function for running grabcut
///
/// @param grabcutMask The Grabcut mask
/// @param cropRect The grabcut crop rect
/// @param currentFrame The current video frame
/// @param error AKA Throws, just weird objective c syntax
-(UIImage*) runGrabcut: (UIImage*)grabcutMask cropRect:(CGRect)cropRect currentFrame:(UIImage*)currentFrame error:(NSError *__autoreleasing *)error
{
    // Convert the video frame to a Mat
    cv::Mat sourceImgMat = [self cvMatFromUIImage:currentFrame];
    
    // Remove the alpha channel
    cv::cvtColor(sourceImgMat , sourceImgMat , CV_RGBA2RGB);
    
    // Convert the grabcut mask to Mat1b
    cv::Mat1b markers = [self cvMatMaskerFromUIImage:grabcutMask];
    
    // Declare rect
    cv::Rect rectangle(0,0,0,0);
    
    // Declare background and foreground model
    cv::Mat bgdModel;
    cv::Mat fgdModel;
    
    // GrabCut segmentation
    cv::grabCut(sourceImgMat, markers, rectangle, bgdModel, fgdModel, 5, cv::GC_INIT_WITH_MASK);
    
    // Declare foreground and pr_foreground mats
    cv::Mat foreground;
    cv::Mat pr_foreground;
    
    // Compare the mats
    cv::compare(markers, cv::Scalar(cv::GC_FGD), foreground, cv::CMP_EQ);
    cv::compare(markers, cv::Scalar(cv::GC_PR_FGD), pr_foreground, cv::CMP_EQ);
    
    // Declare processed mask
    cv::Mat processedMask;
    
    // Add the Foreground and pr_foreground and output it to processed mask
    cv::add(foreground, pr_foreground, processedMask);
    
    // Convert the processed mask into a UIImage and return
    return MatToUIImage(processedMask);
}


/// Function converts a UiImage to a CV::Mat
/// @param image The image to convert
- (cv::Mat)cvMatFromUIImage:(UIImage *)image
{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}

- (cv::Mat1b)cvMatMaskerFromUIImage:(UIImage *) image{
    
    // First get the image into your data buffer
    CGImageRef imageRef = [image CGImage];
    
    // Get the image width and height
    NSUInteger imageWidth = CGImageGetWidth(imageRef);
    NSUInteger imageHeight = CGImageGetHeight(imageRef);
    
    // Create the colour space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Create raw data
    unsigned char *rawData = (unsigned char*) calloc(imageHeight * imageWidth * 4, sizeof(unsigned char));
    
    // Declare how many bytes per pixel
    NSUInteger bytesPerPixel = 4;
    
    // Declare how many bytes per row
    NSUInteger bytesPerRow = bytesPerPixel * imageWidth;
    
    // Declare how many bits per component
    NSUInteger bitsPerComponent = 8;
    
    // Create the CGContext
    CGContextRef context = CGBitmapContextCreate(rawData, imageWidth, imageHeight,bitsPerComponent, bytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    // Release the colour space
    CGColorSpaceRelease(colorSpace);
    
    // Draw the image into the current context
    CGContextDrawImage(context, CGRectMake(0, 0, imageWidth, imageHeight), imageRef);
    
    // Release the context
    CGContextRelease(context);
    
    // Declare markers, this is the return value
    cv::Mat1b markers((int)imageHeight, (int)imageWidth);
    
    // Set the type
    markers.setTo(cv::GC_PR_BGD);
    
    // Get the markers data
    uchar* data =  markers.data;
    
    // Fori image width
    for(int currentX = 0; currentX < imageWidth; currentX++)
    {
        // Fori image height
        for( int currentY = 0; currentY < imageHeight; currentY++)
        {
            // Get the current byte index
            NSUInteger byteIndex = ((image.size.width  * currentY) + currentX ) * 4;
            
            // Get the RGBA values
            UInt8 red   = rawData[byteIndex];
            UInt8 green = rawData[byteIndex + 1];
            UInt8 blue  = rawData[byteIndex + 2];
            UInt8 alpha = rawData[byteIndex + 3];
            
            // If the current colour is black
            if(red == 255 && green == 255 && blue == 255 && alpha == 255)
            {
                // Set foreground
                data[imageWidth*currentY + currentX] = cv::GC_FGD;
            }
            // If the current colour is grey
            else if(red == 220 && green == 220 && blue == 220 && alpha == 255)
            {
                // Set probs foreground
                data[imageWidth*currentY + currentX] = cv::GC_PR_FGD;
            }
            // If the current colour is white
            else if(red == 0 && green == 0 && blue == 0 && alpha == 255)
            {
                // Set background
                data[imageWidth*currentY + currentX] = cv::GC_BGD;
            }
        }
    }
    
    // Free the raw data
    free(rawData);
    
    return markers;
}

@end
