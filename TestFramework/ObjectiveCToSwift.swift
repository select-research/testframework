//
//  ObjectiveCToSwift.swift
//  BVI SDK
//
//  Created by Jake Wade on 23/07/2019.
//  Copyright © 2019 Jake Wade. All rights reserved.
//

import Foundation


/// Protocol for wrapping the BviGrabcutHelper and hiding it from the Framework User
/// This tag allows us to use this protocol in Objective C
@objc(BviGrabcutHelperProtocol)
internal protocol BviGrabcutHelper
{
    /// Declare the default constructor
    init()
    
    /// Function for running grabcut
    ///
    /// - Parameter grabcutMask: The Grabcut mask
    /// - Parameter cropRect: The grabcut crop rect
    /// - Parameter currentFrame: The current video frame
    func runGrabcut(_ grabcutMask: UIImage?, cropRect: CGRect, currentFrame: UIImage?) throws -> UIImage
}

/// Protocol for wrapping the BviGrabcutHelperFactory and hiding it from the Framework User
/// We need a factory to construct the BviGrabcutHelper, as a little bit of reflection is required
/// This tag allows us to use this protocol in Objective C
@objc(BviGrabcutHelperFactory)
internal class BviGrabcutHelperFactory: NSObject
{
    /// The type of the private class that we are going to create
    private static var privateClassType: BviGrabcutHelper.Type!
    
    /// Function registers the private class type
    ///
    /// - Parameter type: The private class type
    @objc static func registerPrivateClassType(type: BviGrabcutHelper.Type)
    {
        // Set the type
        privateClassType = type
    }
    
    /// Function creates an instance of the BviGrabcutHelper
    ///
    /// - Returns: An instance of BviGrabcutHelper
    func createInstance() -> BviGrabcutHelper
    {
        return BviGrabcutHelperFactory.privateClassType.init()
    }
}
