//
//  SwiftToObjectiveC.h
//  BVI SDK
//
//  Created by Jake Wade on 23/07/2019.
//  Copyright © 2019 Jake Wade. All rights reserved.
//

#ifndef SwiftToObjectiveC_h
#define SwiftToObjectiveC_h

#import <TestFramework/TestFramework-Swift.h>
#import <UIKit/UIKit.h>

// Expose internal Swift members
// This code is auto-generated when you make your Swift member public
// You can use it to your advantage to simplify generating this "bridges"

/// This tag pulls the implementation of the Swift protocol into this objective c class
/// Protocol matching BviGrabcutHelperProtocol, so this links back to swift
SWIFT_PROTOCOL_NAMED("BviGrabcutHelperProtocol")
@protocol BviGrabcutHelperProtocol

/// The type of the private class that we are going to create
@property (nonatomic, readonly, copy) NSString * _Nonnull privateProperty;

/// Declare the default constructor
- (nonnull instancetype)init;

/// Function for running grabcut
///
/// @param grabcutMask The Grabcut mask
/// @param cropRect The grabcut crop rect
/// @param currentFrame The current video frame
/// @param error AKA Throws, just weird objective c syntax
- (nonnull UIImage*) runGrabcut: (nonnull UIImage*)grabcutMask cropRect:(CGRect)cropRect currentFrame:(nonnull UIImage*)currentFrame error:(NSError *_Null_unspecified __autoreleasing *_Null_unspecified)error;

@end

// Expose Factory we will use to register classes withing Swift scope
// It actaully needs only one static method to register, passing Class type

/// This tag pulls the implementation of the Swift class into this objective c class
/// Class matching BviGrabcutHelperFactory, so this links back to swift
SWIFT_CLASS("BviGrabcutHelperFactory")
@interface BviGrabcutHelperFactory : NSObject

/// Function registers the private class type
///
/// @param type The private class type
+ (void) registerPrivateClassTypeWithType:(Class <BviGrabcutHelperProtocol> _Nonnull)type;

@end

#endif /* SwiftToObjectiveC_h */
